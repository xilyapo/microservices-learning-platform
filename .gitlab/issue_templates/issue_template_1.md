## Постановка
Здесь надо написать постановку в общих чертах

## Пользовательский путь
Здесь надо прописать то, какие действия сможет выполнять пользователь и к какому результату это приводит. Важно прописать 
все пути, включая те, что ведут к планируемым ошибкам (например, пользователь вводит курс, которого не существует).

## Функциональные требования
1. Тут надо по пунктам прописать, что надо сделать разрабу
2. Можно писать абстрактно
3. Но и можно углубиться и написать тип: напиши метод с логикой - если то, то то. И т.д.

## Дизайн
Если необходим фронт, то можно описать дизайн. В большинстве случаев не будет требоваться

## Критерии успешности
- [ ] Здесь тезисно описывается то, как должна работать система с новыми изменениями, например
- [ ] База данных создана, подключение работает без ошибок
- [ ] Эндпоинт получения курсов (размер - 10 000) отрабатывает быстро (макс - 200мс) при большой нагрузке в 10 000 пользователей
