﻿using LinqToDB;
using microservices_learning_platform.Database;
using microservices_learning_platform.Models.Auth;
using microservices_learning_platform.Models.BlogSystem;
using Microsoft.AspNetCore.Mvc;

namespace microservices_learning_platform.Controllers
{
	/// <summary>
	/// Класс, обеспечивающий получение пользователей
	/// </summary>
	[ApiController]
	[Route("api/users")]
	public class UserController:ControllerBase
	{
		private readonly DatabaseConnection _connection;
		private readonly ILogger<UserController> _logger;
		
		public UserController(DatabaseConnection connection, ILogger<UserController> logger)
		{
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_connection = connection ?? throw new ArgumentNullException(nameof(connection));
		}

		/// <summary>
		/// Получить список всех пользователей
		/// </summary>
		/// <returns>Список пользователей системы</returns>
		[HttpGet]
		public Task<User[]> GetUsers()
		{
			return _connection.User.ToArrayAsync();
		}


		/// <summary>
		/// Получить пользователя по идентификатору
		/// </summary>
		/// <param name="id">Идентификатор пользователя</param>
		/// <returns>Объект пользователя</returns>
		[HttpGet]
		public Task<User?> GetUser(int id)
		{
			return _connection.User.SingleOrDefaultAsync(item => item.Id == id);
		}

		/// <summary>
		/// Получить расширенную информацию о пользователе
		/// </summary>
		/// <param name="id">Идентификатор пользователя</param>
		/// <returns>Объект расширенной информации</returns>
		[HttpGet("info/{id}")]
		public Task<UserInfo?> GetUserInfo(int id)
		{
			return _connection.UserInfo.SingleOrDefaultAsync(item => item.Id == id);
		}
	}
}
