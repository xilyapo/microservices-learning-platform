﻿using LinqToDB;
using microservices_learning_platform.Database;
using microservices_learning_platform.Models.BlogSystem;
using Microsoft.AspNetCore.Mvc;

namespace microservices_learning_platform.Controllers
{
    /// <summary>
    /// Контроллер постов в блоге
    /// </summary>
    [ApiController]
    [Route("api/blogposts")]
    public class BlogPostController : ControllerBase
    {
        private readonly DatabaseConnection _connection;
        private readonly ILogger<BlogPostController> _logger;

        public BlogPostController(ILogger<BlogPostController> logger, DatabaseConnection connection)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
        }

        /// <summary>
        /// Возвращает все посты блога
        /// </summary>
        /// <returns>Список постов блога</returns>
        [HttpGet]
        public Task<BlogPost[]> GetBlogPosts()
        {
            return _connection.BlogPost.ToArrayAsync();
        }

        /// <summary>
        /// Возвращает пост блога по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор поста</param>
        /// <returns>Пост блога</returns>
        [HttpGet("{id}")]
        public Task<BlogPost?> GetSingleBlogPost(int id)
        {
            return _connection.BlogPost.SingleOrDefaultAsync(post => post.Id == id);
        }

    }
}
