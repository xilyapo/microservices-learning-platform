﻿using LinqToDB;
using LinqToDB.Data;
using microservices_learning_platform.Models.Auth;
using microservices_learning_platform.Models.BlogSystem;

namespace microservices_learning_platform.Database
{
    /// <summary>
    /// Подключение к базе данных. Все модели данных прописываются здесь.
    /// </summary>
    public class DatabaseConnection : DataConnection
    {
		public ITable<BlogPost> BlogPost => this.GetTable<BlogPost>();
		public ITable<User> User => this.GetTable<User>();
		public ITable<UserInfo> UserInfo => this.GetTable<UserInfo>();
		public DatabaseConnection(DataOptions<DatabaseConnection> options): base(options.Options)
		{
		
		}
    }
}
