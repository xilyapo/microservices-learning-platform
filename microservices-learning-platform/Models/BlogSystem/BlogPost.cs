﻿using LinqToDB.Mapping;

namespace microservices_learning_platform.Models.BlogSystem
{
    /// <summary>
    /// Пост в блоге
    /// </summary>
    [Table(Name = "blog_post", Schema = "common")]
    public class BlogPost
    {
        /// <summary>
        /// Идентификатор поста
        /// </summary>
        [PrimaryKey, Identity]
        [Column(Name="id")]
        public int Id { get; set; }

        /// <summary>
        /// Заголовок
        /// </summary>
        [Column(Name = "title")]
        public string? Title { get; set; }

        /// <summary>
        /// Текст поста
        /// </summary>
        [Column(Name = "content")]
        public string? Content { get; set; }

        /// <summary>
        /// Тэг для поиска
        /// </summary>
        [Column(Name = "tag")]
        public string? Tag { get; set; }
    }
}
