﻿using LinqToDB.Mapping;

namespace microservices_learning_platform.Models.Auth
{
	/// <summary>
	/// Расширенная информация о пользователе
	/// </summary>
	[Table(Name = "user_info", Schema = "auth")]
	public class UserInfo
	{
		[PrimaryKey, Identity]
		[Column(Name = "id")]
		public int Id { get; set; }

		/// <summary>
		/// Идентификатор пользователя
		/// </summary>
		[Column(Name = "user_id")]
		public int UserId { get; set; }

		/// <summary>
		///  Фамилия
		/// </summary>
		[Column(Name = "fullname")]
		public string FullName { get; set; }

		/// <summary>
		///  Псевдоним
		/// </summary>
		[Column(Name = "nickname")]
		public string Nickname { get; set; }

		/// <summary>
		///  Дата регистрации
		/// </summary>
		[Column(Name = "registration_date")]
		public DateTime RegistrationDate { get; set; }

		/// <summary>
		///  Дата последнего обновления пользователя
		/// </summary>
		[Column(Name = "last_update_date")]
		public DateTime LastUpdateDate { get; set; }

		/// <summary>
		///  Идентификатор пользователя, обновившего данного пользователя
		/// </summary>
		[Column(Name = "last_update_user_id")]
		public int LastUpdateUserId {  get; set; }

		/// <summary>
		///  Дата удаления
		/// </summary>
		[Column(Name = "deletion_date")]
		public DateTime DeletionDate { get; set; }

		/// <summary>
		///  Идентификатор пользователя, удалившего данного пользователя
		/// </summary>
		[Column(Name = "deletion_user_id")]
		public int DeletionUserId { get; set; }

		/// <summary>
		///  Ссылка на аватар
		/// </summary>
		[Column(Name = "avatar_url")]
		public string AvatarUrl { get; set; }

		/// <summary>
		///  Биография
		/// </summary>
		[Column(Name = "bio")]
		public string Bio { get; set; }

		/// <summary>
		///  Объект пользователя
		/// </summary>
		[Association(ThisKey = nameof(UserId), OtherKey = nameof(User.Id))]
		public User? User { get; set; }
	}
}
