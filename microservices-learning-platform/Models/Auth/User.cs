﻿using LinqToDB.Mapping;

namespace microservices_learning_platform.Models.Auth
{
	/// <summary>
	/// Пользователь
	/// </summary>
	[Table(Name = "user", Schema = "auth")]
    public class User
    {
		/// <summary>
		/// Идентификатор записи
		/// </summary>
		[PrimaryKey, Identity]
		[Column(Name = "id")]
		public int Id { get; set; }

		/// <summary>
		/// Логин
		/// </summary>
		[Column(Name = "login")]
		public string Login { get; set; }

		/// <summary>
		/// Электронная почта
		/// </summary>
		[Column(Name = "email")]
		public string Email { get; set; }

		/// <summary>
		/// Пароль
		/// </summary>
		[Column(Name = "password")]
		public string Password { get; set; }

    }
}
