using LinqToDB;
using LinqToDB.AspNet;
using LinqToDB.AspNet.Logging;
using microservices_learning_platform.Database;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

// Configure database connection
builder.Services.AddLinqToDBContext<DatabaseConnection>((provider, options)
    => options
        .UsePostgreSQL(builder.Configuration.GetConnectionString("DbConnectionString") 
            ?? throw new ArgumentNullException("Connection string must be set"))
        .UseDefaultLogging(provider));

// Добавим сервис Swagger
builder.Services.AddSwaggerGen(options =>
{
	var xmlFileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
	options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFileName));
});


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI(options =>
	{
		options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
		options.RoutePrefix = "site/api/docs"; // Путь для открытия SwaggerUI
	});
}

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

// Run the app
app.Run();
