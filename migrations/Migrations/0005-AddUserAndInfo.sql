-- Создать таблицу пользователей системы
CREATE TABLE auth."user" (
    id SERIAL PRIMARY KEY,
    login TEXT,
    email TEXT,
    password TEXT
);

comment on table auth."user" is 'Таблица пользователей системы';
comment on column auth."user".id is 'Идентификатор записи';
comment on column auth."user".login is 'Логин';
comment on column auth."user".email is 'Электронная почта';
comment on column auth."user".password is 'Пароль';

-- Создать таблицу расширенной информации о пользователе
CREATE TABLE auth.user_info (
    id SERIAL PRIMARY KEY,
    user_id INT UNIQUE,
    fullname TEXT,
    nickname TEXT,
	registration_date TIMESTAMP,
	last_update_date TIMESTAMP,
    last_update_user_id INT,
	deletion_date TIMESTAMP,
	deletion_user_id INT,
    avatar_url TEXT,
	bio TEXT,
	CONSTRAINT FK_UserInfo_User FOREIGN KEY (user_id) REFERENCES auth."user" (id) ON DELETE CASCADE
);

comment on table auth.user_info is 'Таблица расширенной информации о пользователе';
comment on column auth.user_info.id is 'Идентификатор записи';
comment on column auth.user_info.user_id is 'Идентификатор пользователя';
comment on column auth.user_info.fullname is 'Полное имя';
comment on column auth.user_info.nickname is 'Псевдоним';
comment on column auth.user_info.registration_date is 'Дата регистрации';
comment on column auth.user_info.last_update_date is 'Дата последнего обновления записи';
comment on column auth.user_info.last_update_user_id is 'Идентификатор пользователя, обновившего запись';
comment on column auth.user_info.deletion_date is 'Дата удаления записи';
comment on column auth.user_info.deletion_user_id is 'Идентификатор пользователя, удалившего запись';
comment on column auth.user_info.avatar_url is 'Ссылка на аватар';
comment on column auth.user_info.bio is 'Биография';


-- Заводим первых пользователей
INSERT INTO auth."user" (login, email, password) VALUES
	('admin','admin@admin.ru','admin'); -- В продакшене пароль должен приходить захешированным!
INSERT INTO auth.user_info (user_id, fullname, nickname, registration_date, avatar_url, bio) VALUES
	(1,'Иванов Иван Иванович','ADMIN', '2022-02-22', 'IMAGE HERE PLS0', 'VERY IMPORTANT PERSON');