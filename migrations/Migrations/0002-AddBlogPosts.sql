﻿-- Создание таблицы для постов блога образовательной платформы

CREATE TABLE common.blog_post (
    id SERIAL PRIMARY KEY,
    title VARCHAR(255),
    content TEXT,
    tag VARCHAR(255)
);

comment on table common.blog_post is 'Таблица постов блога образовательной платформы';
comment on column common.blog_post.id is 'Идентификатор записи';
comment on column common.blog_post.title is 'Заголовок';
comment on column common.blog_post.content is 'Текст блога';
comment on column common.blog_post.tag is 'Тэг';